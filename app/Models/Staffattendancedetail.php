<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staffattendancedetail extends Model
{
    use HasFactory;

    protected $fillable = ['employeeid','empLocation','empattendancestatus','empattendancedate'];

    public function staff()
    {
    	$this->hasOne(Staffdetail::class,'employeeid','employeeid');
    }
    
}
