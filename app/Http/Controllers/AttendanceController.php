<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Staffdetail;
use App\Models\Staffattendancedetail;

class AttendanceController extends Controller
{
    public function saveAttendance($value='')
    {
    	$staffdetails = Staffdetail::orderBy('empfirstname')->get();
    	return view('save-attendance',compact('staffdetails'));
    }

    public function storeAttendance(Request $request)
    {
    	if(Staffattendancedetail::where('employeeid',$request->employeeid)->where('empattendancedate',$request->empattendancedate)->exists())
    	{
    		return back()->with('error','Attendance already added');
    	}
    	else
    	{
    		$staff = Staffdetail::find($request->employeeid);
    		Staffattendancedetail::create([
    									'employeeid'=>$request->employeeid,
    									'empattendancedate'=>$request->empattendancedate,
    									'empattendancestatus'=>$request->empattendancestatus,
    									'empLocation'=>$staff->emplocation
    								]);
    	}

    	return back()->with('success','Attendance saved successfully');
    }

    public function getAttendance(Request $request)
    {
        $staffdetails = Staffdetail::orderBy('empfirstname')->get();

        $attendance = '';

        if(isset($request->employeeid) && isset($request->from_date) && isset($request->to_date))
        {
            if($request->from_date > $request->to_date)
                return back()->with('error','Please select proper from date and to date');

            $attendance = Staffattendancedetail::where('employeeid',$request->employeeid)
                                            ->whereBetween('empattendancedate',[$request->from_date,$request->to_date])
                                            ->get();

            foreach($attendance as $att)
            {
                switch ($att->empattendancestatus) {
                    case 1:
                        # code...
                        $att->status = 'Present';
                        break;
                    case 2:
                        # code...
                        $att->status = 'Absent';
                        break;
                    case 3:
                        # code...
                        $att->status = '--';
                        break;
                    case 4:
                        # code...
                        $att->status = 'N/A';
                        break;
                    default:
                        # code...
                        $att->status = '--';
                        break;
                }
            }
        }

        return view('get-attendance',compact('staffdetails','attendance'));
    }

    // public function getAttendanceList(Request $request)
    // {
    //     if($request->from_date > $request->to_date)
    //         return back()->with('error','Please select proper from date and to date');

    //     $attendance = Staffattendancedetail::with('staff')
    //                                         ->where('employeeid',$request->employeeid)
    //                                         ->whereBetween('empattendancedate',[$request->from_date,$request->to_date])
    //                                         ->get();

    //     return view('get-attendance',compact('attendance'));
    // }
}
